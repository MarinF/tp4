$(document).ready(function(){

	//Q3 - Afficher les ingrédients de la pizza quand on passe la souris
	$('.type.pizza-type label').hover(function(){
		$(this).find("span").removeClass('no-display');
	}, function(){
		$(this).find("span").addClass('no-display');
	});

	//Q4 - Nombre de parts
	$(".type.nb-parts input").change(function(){
		var parts = $(".type.nb-parts input").val();
		//alert(+parts);
		if (parts <7){
			$('span.pizza-pict').removeClass().addClass('pizza-pict').addClass('pizza-'+parts);
		}
	});

	//Q5 - Afficher formulaire
	$('.btn.btn-success.next-step').click(function(){
		$('.infos-client').removeClass("no-display");
		$(this).addClass('no-display');
	});

	//Q6 - New ligne adresse
	$('.btn.add').click(function(){
		$(this).parent().prepend('<input type="text"/><br>');
	});

	//Q7 - New page remerciements
	$('.btn.done').click(function(){
		var nom = $('.infos-client input:first').val();
		$('<h1>Merci ' + nom + '! Votre commande sera livrée dans 15 minutes.</h1>').replaceAll(".main");
	});

	//Q8 - Prix
	var total=0;
	$('input').change(
		
		function(){
			total=0;
			$('input:checked').each(function(){ 
				total += parseInt($(this).attr('data-price'));
				console.log(total);
			});
			$('.stick-right p').html(total +'€');


		});
	




}

);
